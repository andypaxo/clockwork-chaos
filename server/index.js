const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const GameServer = require('./game_server');

const path = require('path');

app.use(express.static(path.join(__dirname, '../public')));
 
app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname, '../public/index.html'));
});

const gameServer = new GameServer(io);
gameServer.start();

server.listen(8081, () => {
  console.log(`Listening on ${server.address().port}`);
});