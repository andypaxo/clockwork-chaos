const planck = require('planck-js');
const Vec2 = planck.Vec2;

const filterCatPlayer = 0x0002;
const filterCatWall = 0x0004;
const filterCatMonster = 0x0008;
const filterCatEnvironment = 0x0010;

const fs = require('fs');
const path = require('path');

class Cooldown {
  constructor(length, startHot) {
    this.lastFired = startHot ? Date.now() : Date.now() - length;
    this.length = length;
  }

  fire(length, override) {
    if (this.isCold() || override) {
      this.lastFired = Date.now();
      this.length = length || this.length;
      return true;
    }

    return false;
  }

  isHot() {
    return Date.now() < this.lastFired + this.length;
  }

  isCold() {
    return Date.now() > this.lastFired + this.length;
  }

  secondsRemaining() {
    return (this.lastFired + this.length - Date.now()) / 1000;
  }

  progress() {
    return Math.min((Date.now() - this.lastFired) / this.length, 1);
  }
}

class Entity {
  constructor() {
    this.size = {x: 18 / 20, y: 18 / 20};
  }

  getInfo() {
    const center = this.body.getWorldCenter();
    const velocity = this.body.getLinearVelocity();
    return {
      id: this.id,
      x: center.x * 10,
      y: center.y * 10,
      direction: {x: velocity.x, y: velocity.y},
      type: this.type
    };
  }

  hurt(amount, source, force, stun) {}
}

class WalkEnemy extends Entity {
  constructor() {
    super();
    this.attackCooldown = new Cooldown(500);
    this.hurtCooldown = new Cooldown(300);
    this.moveCooldown = new Cooldown(0);
    this.health = this.maxHealth = 20;
    this.attackDamage = 10;
    this.walkSpeed = 5;
    this.size = new Vec2(1, 1);
  }

  update(world) {
    if (this.hurtCooldown.isHot()) {
      this.body.setLinearVelocity(Vec2.mul(this.body.getLinearVelocity(), 0.9));
      return;
    }

    if (this.health <= 0) {
      world.destroyEntity(this.id);
    }

    if (this.moveCooldown.fire(500 + Math.random() * 500)) {
      const players = world.getActivePlayers();
      let target = new Vec2(Math.random() * 40, Math.random() * 30);
      // Move toward random player
      if (players.length) {
        const player = players[Math.floor(Math.random() * players.length)];
        target = player.body.getWorldCenter();
      }

      const direction = Vec2.sub(target, this.body.getWorldCenter());
      direction.normalize();
      this.body.setLinearVelocity(direction.mul(this.walkSpeed));
    }

    // Try to attack nearest player
    const pos = this.body.getWorldCenter();
    const nearestPlayer = world.getActivePlayers().sort((a, b) => Vec2.distanceSquared(a.body.getWorldCenter(), pos) - Vec2.distanceSquared(b.body.getWorldCenter(), pos))[0];
    if (nearestPlayer && Vec2.distance(nearestPlayer.body.getWorldCenter(), pos) < 2.4 && this.attackCooldown.fire()) {
      this.attack(pos, nearestPlayer, world);
    }
  }

  attack(pos, nearestPlayer, world) {
    const between = Vec2.mid(pos, nearestPlayer.body.getWorldCenter());
    world.addFX('bang', between.x, between.y);
    nearestPlayer.hurt(this.attackDamage);
  }

  hurt(amount, source, force, stun) {
    this.health -= amount;
    this.hurtCooldown.fire(stun);
    const direction = Vec2.sub(this.body.getWorldCenter(), source);
    direction.normalize();
    this.body.setLinearVelocity(direction.mul(force || 10));
    this.moveCooldown.length = 0;
  }

  getInfo() {
    return Object.assign(super.getInfo(), {
      isHurt: this.hurtCooldown.isHot(),
      isAttacking: this.attackCooldown.isHot()
    });
  }
}

class Imp extends WalkEnemy {
  constructor() {
    super();
    this.type = 'imp';
  }
}

class RoboDemon extends WalkEnemy {
  constructor() {
    super();
    this.type = 'robo-demon';
    this.attackDamage = 20;
    this.walkSpeed = 3;
  }
}

class Tentacle extends Entity {
  constructor() {
    super();
    this.type = 'tentacle';
    this.attackCooldown = new Cooldown(500);
    this.hurtCooldown = new Cooldown(300);
    this.health = this.maxHealth = 20;
    this.attackDamage = 25;
    this.size = {x: 40 / 20, y: 18 / 20};
  }

  update(world) {
    if (this.hurtCooldown.isHot()) {
      return;
    }

    if (this.health <= 0) {
      world.destroyEntity(this.id);
    }

    // Try to attack nearby player
    if (this.attackCooldown.isCold()) {
      const pos = this.body.getWorldCenter();
      let didStrike = false;
      world.physics.queryAABB(
        new planck.AABB(new Vec2(pos.x, pos.y - 3), new Vec2(pos.x + 4, pos.y + 3)),
        fixture => {didStrike = this.tryHurt(fixture, world, false) || didStrike; return true; }
      );
      if (!didStrike) {
        world.physics.queryAABB(
          new planck.AABB(new Vec2(pos.x - 4, pos.y - 3), new Vec2(pos.x, pos.y + 3)),
          fixture => {this.tryHurt(fixture, world, true); return true;}
        );
      }
    }
  }

  tryHurt(fixture, world, flip) {
    const entity = world.players[fixture.getBody().id];
    if (entity) {
      entity.hurt(this.attackDamage);
      this.flip = flip;
      this.attackCooldown.fire();
    }
    return !!entity;
  }

  hurt(amount, _source, _force, stun) {
    this.health -= amount;
    this.hurtCooldown.fire(stun);
  }

  getInfo() {
    return Object.assign(super.getInfo(), {
      isHurt: this.hurtCooldown.isHot(),
      isAttacking: this.attackCooldown.isHot(),
      direction: {x: this.flip ? -1 : 1, y: 0}
    });
  }
}

class Boss1 extends Entity {
  constructor() {
    super();
    this.type = 'boss1';
    this.attackCooldown = new Cooldown(500);
    this.hurtCooldown = new Cooldown(300);
    this.health = this.maxHealth = 300;
    this.attackDamage = 10;
    this.walkSpeed = 5;
    this.attackPhase = 0;
    this.charging = false;
    this.size = new Vec2(1.5, 1.5);
  }

  update(world) {
    if (!this.spawnPosition) {
      this.spawnPosition = this.body.getWorldCenter().clone();
    }
    const time = Date.now() / 1000;
    this.body.setPosition(new Vec2(Math.sin(time * 0.5) * 5, Math.sin(time * 0.8) * 5).add(this.spawnPosition));
    if (this.attackCooldown.fire()) {
      this.attackPhase = (this.attackPhase + 1) % 12;
      if (this.attackPhase < 4) {
        this.fireballAttack(world);
      } else if (this.attackPhase === 6) {
        this.isCharging = true;
      } else if (this.attackPhase === 9) {
        this.isCharging = false;
        world.addSFX('boss1-firestorm');
      }
    }
    if (this.attackPhase === 9) {
      this.explosionAttack(world);
    }
    

    if (this.health <= 0) {
      for (var i = 0; i < 12; i++) {
        this.explosionAttack(world);
      }
      world.destroyEntity(this.id);
      world.addSFX('boss1-die');
    }
  }

  fireballAttack(world) {
    const players = Object.values(world.players);
    if (!players.length) return;

    const targetPlayer = players[this.attackPhase % players.length];
    world.addEntity(new Boss1Fireball(this.body.getWorldCenter().clone(), targetPlayer.body.getWorldCenter().clone()));
    world.addSFX('boss1-fireball');
  }

  explosionAttack(world) {
    const angle = Math.random() * Math.PI * 2;
    const position = new Vec2(Math.sin(angle), Math.cos(angle)).mul(2 + Math.random()).add(this.body.getWorldCenter());
    world.addFX('fx-boss1explosion', position.x, position.y);
    
    world.physics.queryAABB(
      new planck.AABB(new Vec2(position.x - 1, position.y - 1), new Vec2(position.x + 1, position.y + 1)),
      fixture => {
        const entity = world.players[fixture.getBody().id];
        entity && entity.hurt(this.attackDamage);
      }
    );
  }

  hurt(amount, _source, _force, stun) {
    this.health -= amount;
    this.hurtCooldown.fire(stun);
  }

  getInfo() {
    return Object.assign(super.getInfo(), {
      isHurt: this.hurtCooldown.isHot(),
      isAttacking: this.attackCooldown.isHot(),
      isCharging: this.isCharging
    });
  }
}

class Boss1Fireball extends Entity {
  constructor(position, target) {
    super();
    this.type = 'boss1fireball';
    this.position = position;
    this.direction = target.sub(position);
    this.direction.normalize();
    this.direction.mul(10);
  }

  update(world) {
    if (!this.bodyConfigured) {
      this.bodyConfigured = true;
      this.body.setLinearVelocity(this.direction);
      const fixture = this.body.getFixtureList();
      fixture.setFilterCategoryBits(filterCatEnvironment);
      fixture.setFilterMaskBits(filterCatPlayer | filterCatWall);
      fixture.setSensor(true);
    }
  }

  contact(other, world) {
    if (world.players[other.id]) {
      world.players[other.id].hurt(15);
      this.destroy(world);
    } else if (other.getFixtureList().getFilterCategoryBits() === filterCatWall) {
      this.destroy(world);
    }
  }

  destroy(world) {
    world.destroyEntity(this.id);
    const position = this.body.getWorldCenter();
    world.addFX('fx-boss1explosion', position.x, position.y);
  }
}

class WalkBomb extends Entity {
  constructor() {
    super();
    this.type = 'walkBomb';
    this.explodeCooldown = new Cooldown(1000 + Math.random() * 500, true);
  }

  update(world) {
    if (!this.bodyConfigured) {
      this.bodyConfigured = true;
      this.body.setLinearVelocity(this.direction);
    }
    if (this.explodeCooldown.fire()) {
      const pos = this.body.getWorldCenter();
      world.physics.queryAABB(
        new planck.AABB(new Vec2(pos.x - 4, pos.y - 4), new Vec2(pos.x + 4, pos.y + 4)),
        fixture => {
          const entity = world.entities[fixture.getBody().id];
          entity && entity.hurt && entity.hurt(20, pos, 40);
        }
      );
      world.destroyEntity(this.id);
      world.addFX('fx-bomb-explode', pos.x, pos.y);
      world.addSFX('attack-zygur');
    }
  }
}

class Zombie extends Entity {
  constructor() {
    super();
    this.type = 'zombie';
    this.deathCooldown = new Cooldown(6000, true);
    this.attackCooldown = new Cooldown(500);
    this.attackCooldown.lastFired = Date.now() - Math.random() * 500;
    this.isPlayerControlled = true;
  }

  update(world) {
    if (this.deathCooldown.fire()) {
      world.destroyEntity(this.id);
    }
    if (this.attackCooldown.fire()) {
      const pos = this.body.getWorldCenter();
      if (!this.target || !this.target.health) {
        this.target = Object.values(world.entities)
          .filter(ent => !!ent.body && ent.type !== 'zombie')
          .sort((a, b) => Vec2.distanceSquared(a.body.getWorldCenter(), pos) - Vec2.distanceSquared(b.body.getWorldCenter(), pos))
          [0];
      }
      if (this.target) {
        const targetPos = this.target.body.getWorldCenter();
        const direction = Vec2.sub(targetPos, pos);
        direction.normalize();
        this.body.setLinearVelocity(direction.mul(4));
        if (Vec2.distance(pos, targetPos) < 2) {
          this.target.hurt(10, pos);
        }
      }
    }
  }

  hurt(_amount, source, force) {
    const direction = Vec2.sub(this.body.getWorldCenter(), source);
    direction.normalize();
    this.body.setLinearVelocity(direction.mul(force || 10));
  }
}

class Player {
  constructor() {
    this.maxHealth = this.health = 100;
    this.attackCooldown = new Cooldown(500);
    this.hurtCooldown = new Cooldown(300);
    this.specialCooldown = new Cooldown(10000);
    this.specialCooldown.lastFired = Date.now() - 500;
    this.direction = { x: 1, y: 0 };
    this.input = {left: false, right: false, up: false, down: false, attack: false, special: false};
    this.wasHurt = false;
    this.specials = {
      arx: (world) => {
        const pos = this.body.getWorldCenter();
        const flip = this.direction.x < 0;
        world.physics.queryAABB(
          new planck.AABB(new Vec2(flip ? pos.x - 12 : pos.x, pos.y - 12), new Vec2(flip ? pos.x : pos.x + 12, pos.y + 4)),
          fixture => {
            const entity = world.entities[fixture.getBody().id];
            entity && entity.hurt && entity.hurt(10, pos, 50, 800);
          }
        );
        world.addFX('fx-arx-force', pos.x + (flip ? -6 : 6), pos.y - 4, {flip});
        world.addSFX('attack-arx');
      },
      cornelius: (world) => {
        this.coinsCooldown = new Cooldown(1500, true);
        world.addSFX('attack-cornelius');
      },
      flint: (world) => {
        const position = this.body.getWorldCenter();
        world.addEntity(Object.assign(new Zombie, {position: Vec2.add(position, new Vec2(Math.random() * 4 - 2, Math.random() * 4 - 2))}));
        world.addEntity(Object.assign(new Zombie, {position: Vec2.add(position, new Vec2(Math.random() * 4 - 2, Math.random() * 4 - 2))}));
        world.addSFX('attack-flint');
      },
      zygur: (world) => {
        const position = this.body.getWorldCenter();
        const flip = this.direction.x < 0;
        const xDir = flip ? -1 : 1;
        [new Vec2(6 * xDir, 0), new Vec2(5.5 * xDir, 5.5), new Vec2(5.5 * xDir, -5.5)]
          .forEach(direction =>  world.addEntity(Object.assign(new WalkBomb, {position, direction})));
      },
    }
  }

  update(world) {
    if (this.health <= 0) {
      if(this.rezCooldown.isCold()) {
        this.health = 100;
        this.specialCooldown.fire(undefined, true);
        // this.body.setPosition(new Vec2(5, 10 + Math.random() * 10));
        delete this.rezCooldown;
      }
      this.body.setLinearVelocity(new Vec2);
      return;
    }

    const input = this.input;
    const direction = {
      x: input.left ? -1 : input.right ? 1 : 0,
      y: input.up ? -1 : input.down ? 1 : 0
    };
    this.body.setLinearVelocity(new Vec2(direction.x * 15, direction.y * 15).clamp(15));
    this.direction = {
      x: direction.x === 0 ? this.direction.x : direction.x,
      y: direction.y === 0 ? this.direction.y : direction.y,
    }
    this.isMoving = direction.x !== 0 || direction.y !== 0;

    if (input.attack && this.attackCooldown.fire()) {
      const pos = this.body.getWorldCenter();
      const flip = this.direction.x < 0;
      world.addFX('fx-sword', this.direction.x, 0, {flip, parent: this.id});
      world.physics.queryAABB(
        new planck.AABB(new Vec2(flip ? pos.x - 3 : pos.x, pos.y - 3), new Vec2(flip ? pos.x : pos.x + 3, pos.y + 3)),
        fixture => {
          const entity = world.entities[fixture.getBody().id];
          entity && entity.hurt && entity.hurt(6, pos);
        }
      );
    }

    if (this.coinsCooldown && this.coinsCooldown.isHot()) {
      const pos = Vec2.add(this.body.getWorldCenter(), new Vec2(Math.random() * 16 - 8, Math.random() * 16 - 8));
      world.addFX('fx-coin', pos.x, pos.y);
      world.addFX('fx-coin-shadow', pos.x, pos.y + .5);
      world.physics.queryAABB(
        new planck.AABB(new Vec2(pos.x - 1.5, pos.y - 1.5), new Vec2(pos.x + 1.5, pos.y + 1.5)),
        fixture => {
          const entity = world.entities[fixture.getBody().id];
          entity && entity.hurt && entity.hurt(5, pos);
        }
      );
    }

    if (input.special && this.specialCooldown.fire()) {
      this.specials[this.character](world);
    }
  }

  hurt(amount) {
    if (this.hurtCooldown.fire() && this.health > 0) {
      this.health -= amount;
      this.wasHurt = true;
      if (this.health <= 0 && !this.rezCooldown) {
        this.rezCooldown = new Cooldown(8000);
        this.rezCooldown.fire(undefined, true);
      }
    }
  }

  getInfo() {
    const center = this.body.getWorldCenter();
    const result = {
      id: this.id,
      character: this.character,
      x: center.x * 10,
      y: center.y * 10,
      direction: this.direction,
      isMoving: this.isMoving,
      health: this.health / this.maxHealth,
      type: 'player',
      isContainer: true, // This sucks. Should be client side decision based on type
      isHurt: this.hurtCooldown.isHot(),
      wasHurt: this.wasHurt,
      specialCharge: this.specialCooldown.progress(),
      startFade: !!this.startFade
    };
    if (this.rezCooldown) {
      result.secondsToRez = Math.floor(this.rezCooldown.secondsRemaining())
    }
    this.wasHurt = false;
    return result;
  }
}

class World {
  constructor() {
    this.players = {};
    this.entities = {};
    this.limits = {};
    this.fx = [];
    this.sfx = [];
    this.static = [];
    this.spawnpoints = {};
    this.physics = new planck.World;
    this.nextId = 1;
    this.limit = null;

    this.physics.on('begin-contact', contact => {
      const ba = contact.getFixtureA().getBody(), bb = contact.getFixtureB().getBody();
      this.entities[ba.id] && this.entities[ba.id].contact && this.entities[ba.id].contact(bb, this);
      this.entities[bb.id] && this.entities[bb.id].contact && this.entities[bb.id].contact(ba, this);

      ba.contactBody && ba.contactBody(bb);
      bb.contactBody && bb.contactBody(ba);
    });

    this.loadLevel(1);

    this.areas = [
      {
        waves: [
          () => { this.spawnWave(1, () => new Imp, 'db'); this.setLimit('limit0'); },
          () => { this.spawnWave(3, () => new Imp, 'da'); this.setLimit('limit1'); },
          () => { this.spawnWave(2, () => new RoboDemon, 'db'); this.setLimit('limit2'); },
          () => { this.spawnWave(2, () => new RoboDemon, 'de'); this.spawnWave(3, () => new Imp, 'de') },
          () => { this.spawnWave(3, () => new Imp, 'dc'); this.spawnWave(1, () => new RoboDemon, 'dd1'); this.spawnWave(1, () => new RoboDemon, 'dd2'); this.setLimit('limit3') },
          () => { this.spawnWave(5, () => new Imp, 'a') },
          () => { this.spawnWave(4, () => new RoboDemon, 'b'); this.spawnWave(1, () => new Tentacle, 'tent1'); this.setLimit('limit4') },
          () => { this.spawnWave(7, () => new Imp, 'c'); this.spawnWave(1, () => new Tentacle, 'tent2') },
          () => { this.spawnWave(6, () => new Imp, 'd'); this.spawnWave(8, () => new Imp, 'e'); this.setLimit('limit5') },
          () => { this.spawnWave(1, () => new Boss1, 'boss'); this.setLimit('limit5')},
          () => { this.setLimit('limit6')},
        ]
      }
    ];
    this.area = 0;
    this.wave = 0;
  }

  loadLevel(levelNum) {
    const data = JSON.parse(fs.readFileSync(path.join(__dirname, `./level${levelNum}.json`)));
    const createFixture = (obj) => {
      return this.physics
        .createBody({position: new Vec2((obj.x + obj.width / 2) / 10, (obj.y + obj.height / 2) / 10)})
        .createFixture(planck.Box(obj.width / 20, obj.height / 20), {filterCategoryBits: filterCatWall, filterMaskBits: filterCatPlayer | filterCatMonster |  filterCatEnvironment});
    };

    data.layers[0].objects.forEach(obj => {
      switch (obj.type) {
        case 'block':
          createFixture(obj);
          break;
        case 'limit':
          this.limits[obj.name] = {
            scrollLimit: obj.x,
            block: createFixture(obj).getBody()
          };
          break;
        case 'spawnpoint':
          this.spawnpoints[obj.name] = {position: new Vec2((obj.x + obj.width / 2) / 10, (obj.y + obj.height / 2) / 10), radius: obj.width / 20};
          break;
        case 'trigger-fade':
          const fixture = createFixture(obj);
          fixture.setSensor(true);
          fixture.getBody().contactBody = other => {
            const player = this.players[other.id];
            if (player) player.startFade = true;
          };
          break;
        default: // tile
          const tileProps = obj.gid >= 37 ? {image: 'tiles64', frame: obj.gid - 37} : {image: 'bg-tiles', frame: obj.gid - 1};
          this.static.push(Object.assign(tileProps, {x: (obj.x + obj.width / 2) / 10, y: (obj.y - obj.height / 2) / 10, type: 'bg'}));
          break;
      }
    });
  }

  setLimit(name) {
    if (this.limit) {
      this.physics.destroyBody(this.limit.block);
    }
    this.limit = this.limits[name];
  }

  getActivePlayers() {
    return Object.values(this.players).filter(player => player.health > 0);
  }

  addPlayer(id, character) {
    const player = this.players[id] = Object.assign(new Player, {
      id, character,
      body: this.physics.createBody({
        position: this.pointFromSpawn('players'),
        fixedRotation: true,
        type: 'dynamic'
      })
    });
    player.body.createFixture(planck.Box(24 / 20, 24 / 20), {filterCategoryBits: filterCatPlayer, filterMaskBits: filterCatWall | filterCatEnvironment});
    player.body.id = id;
  }

  addEntity(template, spawnpoint) {
    console.log(`adding ${template.constructor.name}`)
    const position = template.position || this.pointFromSpawn(spawnpoint);
    const id = this.nextId++;
    const entity = Object.assign(template, {
      id,
      body: this.physics.createBody({
        position,
        fixedRotation: true,
        type: 'dynamic',
        linearDamping: false
      }),
    });
    entity.body.createFixture(planck.Box(template.size.x, template.size.y), {filterCategoryBits: filterCatMonster, filterMaskBits: filterCatWall});
    entity.body.id = id;
    this.entities[entity.id] = entity;
    
    return entity;
  }

  pointFromSpawn(pointname) {
    const spawnpoint = this.spawnpoints[pointname];
    const radius = spawnpoint.radius;
    return Vec2.add(
      new Vec2(Math.random() * radius * 2 - radius, Math.random() * radius * 2 - radius),
      spawnpoint.position
    );
  }

  addFX(type, x, y, attrs) {
    const ext = attrs || {}
    this.fx.push(Object.assign({type, x: x * 10, y: y * 10}, ext));
  }

  addSFX(type) {
    this.sfx.push(type);
  }

  removePlayer(id) {
    if (!this.players[id]) return;
    this.players[id].body && this.physics.destroyBody(this.players[id].body);
    delete this.players[id];
  }

  update() {
    // Spawn new wave if no entities. This will have to check for specific ones if we add more types.
    if (!Object.keys(this.entities).filter(ent => !ent.isPlayerControlled).length) {
      const wave = (this.areas[this.area] || []).waves[this.wave];
      if (wave) {
        console.log(`Starting wave ${this.wave}`);
        wave();
        this.wave++;
      }
    }

    // Update all entites
    Object.values(this.players).forEach((player) => player.update(this));
    Object.values(this.entities).forEach(entity => {
      try {
        entity.update(this);
      } catch (err) {
        console.error(`Couldn't update ${entity}: ${err}`);
      }
    });

    this.physics.step(1 / 30, 6, 2);
  }

  clearFX() { 
    this.fx = [];
    this.sfx = [];
  }

  destroyEntity(id) {
    const entity = this.entities[id];
    this.physics.destroyBody(entity.body);
    delete this.entities[id];
  }

  spawnWave(num, type, spawnpoint) {
    console.log(`Spawning ${num} entities`);
    for (var i = 0; i < num; i++) this.addEntity(type(), spawnpoint);
  }

  nuke() {
    Object.keys(this.entities).forEach(ent => this.destroyEntity(ent));
  }
}

class GameServer {
  constructor(io) {
    this.io = io;
    this.world = new World;

    io.on('connection', (socket) => {
      console.log('a user connected');

      socket.emit('connected', {id: socket.id});
      socket.emit('updateStatic', {objects: this.world.static.map(obj => Object.assign({}, obj, {x: obj.x * 10, y: obj.y * 10}))});
      socket.on('playerInput', inputData => {
        if (this.world.players[socket.id])
          this.world.players[socket.id].input = inputData;
      });
      socket.on('playerSelect', options => this.world.addPlayer(socket.id, options.character));
      socket.on('gmfunc', msg => this.world[msg]());

      socket.on('disconnect', () => {
        console.log('user disconnected');
        this.world.removePlayer(socket.id);
      });
    });
  }

  start() {
    setInterval(() => this.update(), 1000 / 30);
  }

  update() {
    this.world.update();
    this.io.emit('update', {
      entities: Object.values(this.world.entities).map(ent => ent.getInfo()).concat(Object.values(this.world.players).map(ent => ent.getInfo())),
      fx: this.world.fx,
      sfx: this.world.sfx,
      scrollLimit: this.world.limit.scrollLimit
    });
    this.world.clearFX();
  }
}

module.exports = GameServer;