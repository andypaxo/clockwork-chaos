var config = {
  type: Phaser.AUTO,
  parent: 'phaser-example',
  width: 400,
  height: 300,
  zoom: 2,
  scene: {
    preload: preload,
    create: create,
    update: update
  }
};
const isGM = document.location.hash === '#gm';
 
const _game = new Phaser.Game(config);
class Game {
  constructor(phaser) {
    this.phaser = phaser;
    
    this.fx = phaser.add.group();
    if (!isGM) {
      this.healthBarSprite = phaser.add.image(18, 275, 'health-fill').setOrigin(0, 0).setScrollFactor(0).setDepth(30100);
      this.specialSprite = phaser.add.sprite(18 + 201, 275 - 2, 'special-fill').setOrigin(0, 0).setScrollFactor(0).setDepth(30101);
    }

    this.entities = {};
    this.sprites = {
      player: {
        name: 'player',
        animation: ent => ent.character + (ent.health > 0 && ent.isMoving ? '-walk' : '-idle'),
        visible: (ent, game) => ent.health <= 0 ? !(game.updates % 4) : ent.isHurt ? game.updates % 2 : true,
        rotation: ent => ent.health > 0 ? 0 : Math.PI * .5
      },
      imp: {
        name: 'imp',
        animation: () => 'fly',
        visible: (ent, game) => ent.isHurt ? game.updates % 2 : true
      },
      'robo-demon': {
        name: 'robo-demon',
        animation: ent => ent.isAttacking ? 'robo-demon-attack' : 'robo-demon-walk',
        visible: (ent, game) => ent.isHurt ? game.updates % 2 : true
      },
      'tentacle': {
        name: 'tentacle',
        animation: ent => ent.isAttacking ? 'tentacle-attack' : 'tentacle-idle',
        visible: (ent, game) => ent.isHurt ? game.updates % 2 : true
      },
      walkBomb: {
        name: 'walk-bomb',
        animation: () => 'bomb-walk'
      },
      zombie: {
        name: 'zombie',
        animation: () => 'zombie-walk'
      },
      boss1: {
        name: 'boss1',
        animation: () => 'boss1-fly',
        visible: (ent, game) => ent.isHurt ? game.updates % 2 : true,
        tint: (ent, game) => ent.isCharging && !(game.updates % 3) ? 0xff0000 : 0xffffff,
      },
      boss1fireball: {
        name: 'boss1fireball',
        animation: () => 'boss1fireball',
      }
    };
    this.fxSettings = {
      bang: { lifetime: 300 },
      'fx-sword': { lifetime: 300 },
      'fx-arx-force': { lifetime: 176 },
      'fx-bomb-explode': { lifetime: 180 },
      'fx-coin': { lifetime: 500, additional: (sprite, phaser) => {
        const y = sprite.y;
        sprite.y -= 50;
        phaser.add.tween({targets: sprite, y, duration: 500, ease: 'Bounce.Out'});
      } },
      'fx-coin-shadow': { lifetime: 500 },
      'fx-boss1explosion': { lifetime: 176 },
    };
    this.characters = {
      zygur: {},
      cornelius: {},
      flint: {},
      arx: {},
    }
    this.frame = this.updates = 0;
    this.gmView = { x: 300, y: 200 };

    this.setMode(isGM ? 'gm' : 'menu');

    this.cameraHasBeenSetUp = false;
  }

  update(update) {
    this.updates++;
    // Add, remove and update all entities
    this.entities = this.updateCollection(update.entities, this.entities);

    const now = Date.now();
    update.fx.forEach(effect => {
      const sprite = this.phaser.add.sprite(effect.x, effect.y, effect.type).setDepth(10000);
      if (effect.parent) {
        this.entities[effect.parent].add(sprite);
      }
      const fx = this.fxSettings[effect.type];
      if (this.phaser.anims.get(effect.type)) sprite.play(effect.type);
      sprite.expires = now + fx.lifetime;
      sprite.setFlipX(!!effect.flip);
      if (fx.additional) {
        fx.additional(sprite, this.phaser);
      }
      this.fx.add(sprite);
    });
    update.sfx.forEach(sound => this.phaser.sound.play(sound));

    this.fx.children.entries.forEach(fx => fx.expires < now && fx.destroy());
    
    if (!isGM) {
      this.phaser.cameras.main.setBounds(0, -50, update.scrollLimit, 512);
    }
  }

  updateCollection(update, collection) {
    const newEnts = {};
    update.forEach((ent) => {
      let entity = collection[ent.id];
      const spriteSettings = this.sprites[ent.type];

      if (!entity) {
        if (ent.isContainer) {
          entity = this.phaser.add.container(ent.x, ent.y);
          entity.add(this.phaser.add.sprite(0, 0, spriteSettings.name));
        } else {
          entity = this.phaser.add.sprite(ent.x, ent.y, spriteSettings.name);
        }
        newEnts[ent.id] = entity;
        entity.id = ent.id;
      } else {
        newEnts[entity.id] = entity;
        delete collection[entity.id];
      }

      entity.setPosition(ent.x, ent.y);
      entity.depth = entity.y + entity.height / 2;
      const sprite = entity.list ? entity.list[0] : entity;
      sprite.play(spriteSettings.animation(ent), true);
      sprite.setFlipX(ent.direction?.x < 0);
      if (spriteSettings.visible) {
        sprite.setVisible(spriteSettings.visible(ent, this));
      }
      if (spriteSettings.rotation) {
        sprite.setRotation(spriteSettings.rotation(ent));
      }
      if (spriteSettings.tint) {
        sprite.tint = spriteSettings.tint(ent, this);
      }

      // Special case player
      if (ent.id === this.playerId) {
        this.healthBarSprite.displayWidth = Math.max(ent.health * 53, 0);
        this.setMode(ent.health > 0 ? 'game' : 'ko');
        if (ent.health <= 0 && this.tempObjects.countdownText) {
          this.tempObjects.countdownText.text = ent.secondsToRez ? (ent.secondsToRez + '...') : 'ready...';
        } else {
          this.specialSprite.setCrop(0, 0, this.specialSprite.width * ent.specialCharge, this.specialSprite.height);
          if (ent.specialCharge === 1) {
            this.specialSprite.play('special-flash', true)
          } else {
            this.specialSprite.anims.stop();
            this.specialSprite.setFrame(0);
          }
        }
        if (ent.wasHurt) {
          console.log('*** ouch ***');
          this.phaser.sound.play('hurt');
        }
        if (!this.cameraHasBeenSetUp) {
          this.phaser.cameras.main.startFollow(entity, true, 0.6, 0.6, 0, 0);
          this.cameraHasBeenSetUp = true;
        }
        if (ent.startFade && !this.screenFader) {
          this.screenFader = new Tweener(
            inverseLinearTween(2),
            v => this.phaser.cameras.main.alpha = v
          )
        }
      }
    });
    Object.values(collection).forEach(ent => ent.destroy());
    return newEnts;
  }

  updateStatic(update) {
    update.objects.forEach(obj => {
      this.phaser.add.image(obj.x, obj.y, obj.image, obj.frame || 0);
    });
  }

  localUpdate() {
    this.frame++;
    switch (this.mode) {
      case 'menu':
        const time = (Date.now() - this.menuStart) / 1000;
        this.tempObjects.selectChar.setVisible(time > 1 && (time * 1.5) % 2 < 1);
        break;
      case 'gm':
        this.phaser.cameras.main.centerOn(this.gmView.x, this.gmView.y);
    }
  }

  setMode(mode) {
    if (this.mode === mode) return;

    console.log('Setting mode to ' + mode);

    this.teardownTempUI();
    switch (mode) {
      case 'game':
        if (this.mode !== 'ko') {
          this.teardownBGM();
          this.setupGame();
        }
        break;
      case 'ko':
        this.setupKOScreen();
        break;
      case 'menu':
        this.setupMenu();
        break;
      case 'gm':
        this.setupGMControls();
        break;
    }
    this.tempUI.setDepth(40000, 1);

    this.mode = mode;
  }

  teardownTempUI() {
    if (this.tempUI) {
      this.tempUI.destroy(true);
    }
    this.tempUI = this.phaser.add.group();

    this.tempObjects = {};
  }

  teardownBGM() {
    if (this.bgm) {
      this.bgm.forEach(loop => this.phaser.sound.remove(loop));
    }
  }

  setupMenu() {
    let selectHighlight, bigTitle;
    this.menuStart = Date.now();
    this.tempUI.add(this.phaser.add.image(0, 0, 'title-bg').setOrigin(0, 0));
    this.tempUI.add(bigTitle = this.phaser.add.image(200, -25, 'title-big-title'));
    this.phaser.add.tween({targets: bigTitle, y: 95, duration: 1200, ease: 'Bounce.Out'});
    this.tempUI.add(this.tempObjects.selectChar = this.phaser.add.image(200, 170, 'title-select-char'));
    this.tempUI.add(selectHighlight = this.phaser.add.image(200, 170, 'title-select-highlight').setVisible(false));
    Object.keys(this.characters).forEach((character, i) => {
      this.tempUI.add(
        this.phaser.add.sprite(130 + Math.floor(i / 2) * 200, 210 + i % 2 * 60, 'title-char-buttons')
        .setFrame(i).setInteractive()
        .addListener('pointerover', function() { selectHighlight.setPosition(this.x - 20, this.y).setVisible(true); })
        .addListener('pointerout', function() { selectHighlight.setVisible(false); })
        .addListener('pointerup', () => {
          this.selectedCharacter = character;
          this.phaser.sound.play(character);
        })
      );
    });
    
    const bgmloop = this.phaser.sound.add('menuloop', {loop: true});
    this.bgm = [bgmloop];
    setTimeout(() => this.mode === 'menu' && bgmloop.play(), 2000);
    setTimeout(() => this.phaser.sound.play('introshout'), 500);
  }

  setupGame() {
    const bgmloop = this.phaser.sound.add('gameloop1', {loop: true});
    this.bgm = [bgmloop];
    setTimeout(() => {
      bgmloop.play();
      new Tweener(
        inverseLinearTween(30),
        v => bgmloop.volume = .4 + v * .6
      );
    }, 600);
  }

  setupKOScreen() {
    this.tempUI.add(this.phaser.add.bitmapText(200, 100, 'komika24', 'ko! Get back in the fight!').setOrigin(0.5, 0.5).setScrollFactor(0));
    this.tempUI.add(this.tempObjects.countdownText = this.phaser.add.bitmapText(200, 150, 'komika52', '3...').setOrigin(0.5, 0.5).setScrollFactor(0));
  }

  setupGMControls() {
    // this.phaser.scale.setZoom(1);
    this.phaser.scale.setGameSize(800, 600);
  }
}
let game;

function preload() {
  this.load.spritesheet('player', './assets/characters.png', {frameWidth: 24, frameHeight: 24});
  this.load.spritesheet('imp', './assets/imp.png', {frameWidth: 18, frameHeight: 18})
  this.load.spritesheet('robo-demon', './assets/robo-demon.png', {frameWidth: 28, frameHeight: 45})
  this.load.spritesheet('tentacle', './assets/tentacle.png', {frameWidth: 96, frameHeight: 42})
  this.load.spritesheet('zombie', './assets/zombie.png', {frameWidth: 24, frameHeight: 24})
  this.load.spritesheet('boss1', './assets/boss1.png', {frameWidth: 64, frameHeight: 64})
  this.load.spritesheet('boss1fireball', './assets/boss1fireball.png', {frameWidth: 22, frameHeight: 22})
  this.load.spritesheet('fx-boss1explosion', './assets/fx-boss1explosion.png', {frameWidth: 16, frameHeight: 16})
  this.load.spritesheet('walk-bomb', './assets/walk-bomb.png', {frameWidth: 16, frameHeight: 23})
  this.load.spritesheet('fx-sword', './assets/fx-sword.png', {frameWidth: 30, frameHeight: 32})
  this.load.image('bang', './assets/bang.png');
  this.load.spritesheet('bg-tiles', './assets/bg-tiles.png', {frameWidth: 256, frameHeight: 256});
  this.load.spritesheet('tiles64', './assets/tiles64.png', {frameWidth: 64, frameHeight: 64});

  this.load.image('health-bar', './assets/health-bar.png');
  this.load.image('health-fill', './assets/health-fill.png');
  this.load.image('mute', './assets/mute.png');

  this.load.spritesheet('fx-arx-force', './assets/arx-force.png', {frameWidth: 200, frameHeight: 125});
  this.load.spritesheet('fx-bomb-explode', './assets/fx-bomb-explode.png', {frameWidth: 72, frameHeight: 70});
  this.load.spritesheet('fx-coin', './assets/fx-coin.png', {frameWidth: 16, frameHeight: 16});
  this.load.image('fx-coin-shadow', './assets/fx-coin-shadow.png');

  this.load.image('title-bg', './assets/title-bg.png');
  this.load.image('title-select-char', './assets/title-select-char.png');
  this.load.image('title-select-highlight', './assets/title-select-highlight.png');
  this.load.image('title-big-title', './assets/title-big-title.png');
  this.load.spritesheet('title-char-buttons', './assets/title-char-buttons.png', {frameWidth: 200, frameHeight: 50});
  this.load.spritesheet('special-fill', './assets/special-fill.png', {frameWidth: 77, frameHeight: 18})

  this.load.bitmapFont('komika24', './assets/komika24_0.png', './assets/komika24.fnt');
  this.load.bitmapFont('komika52', './assets/komika52_0.png', './assets/komika52.fnt');

  this.load.audio('menuloop', ['./assets/menuloop.mp3']);
  this.load.audio('gameloop1', ['./assets/gameloop1.mp3']);
  this.load.audio('introshout', ['./assets/introshout.mp3']);
  this.load.audio('hurt', ['./assets/hurt.wav']);
  this.load.audio('arx', ['./assets/arx.mp3']);
  this.load.audio('cornelius', ['./assets/cornelius.mp3']);
  this.load.audio('flint', ['./assets/flint.mp3']);
  this.load.audio('zygur', ['./assets/zygur.mp3']);
  this.load.audio('attack-arx', ['./assets/attack-arx.wav']);
  this.load.audio('attack-cornelius', ['./assets/attack-cornelius.wav']);
  this.load.audio('attack-flint', ['./assets/attack-flint.wav']);
  this.load.audio('attack-zygur', ['./assets/attack-zygur.wav']);
  this.load.audio('boss1-fireball', ['./assets/boss1-fireball.wav']);
  this.load.audio('boss1-firestorm', ['./assets/boss1-firestorm.wav']);
  this.load.audio('boss1-die', ['./assets/boss1-die.wav']);

  // this.load.scenePlugin(
  //   'PhaserDebugDrawPlugin',
  //   'https://cdn.jsdelivr.net/npm/phaser-plugin-debug-draw@5.0.0',
  //   'debugDraw',
  //   'debugDraw'
  // );
}
 
function create() {
  game = new Game(this);

  Object.keys(game.characters).forEach((character, idx) => {
    this.anims.create({
      key: character + '-idle',
      frames: this.anims.generateFrameNumbers('player', { frames: [idx * 3] }),
      frameRate: 8,
      repeat: -1
    });
    this.anims.create({
      key: character + '-walk',
      frames: this.anims.generateFrameNumbers('player', { start: idx * 3, end: idx * 3 + 2 }),
      frameRate: 8,
      repeat: -1
    });
  });
  this.anims.create({
    key: 'zombie-walk',
    frames: this.anims.generateFrameNumbers('zombie', { start: 0, end: 2 }),
    frameRate: 8,
    repeat: -1
  });

  this.anims.create({
    key: 'fly',
    frames: this.anims.generateFrameNumbers('imp', { frames: [0, 1] }),
    frameRate: 8,
    repeat: -1
  });
  this.anims.create({
    key: 'robo-demon-walk',
    frames: this.anims.generateFrameNumbers('robo-demon', { frames: [0, 1] }),
    frameRate: 8,
    repeat: -1
  });
  this.anims.create({
    key: 'robo-demon-attack',
    frames: this.anims.generateFrameNumbers('robo-demon', { frames: [2, 0] }),
    frameRate: 8,
  });
  this.anims.create({
    key: 'tentacle-idle',
    frames: this.anims.generateFrameNumbers('tentacle', { frames: [0, 1] }),
    frameRate: 8,
    repeat: -1
  });
  this.anims.create({
    key: 'tentacle-attack',
    frames: this.anims.generateFrameNumbers('tentacle', { frames: [2, 0] }),
    frameRate: 8,
  });
  this.anims.create({
    key: 'boss1-fly',
    frames: this.anims.generateFrameNumbers('boss1', { frames: [0, 1, 2, 3] }),
    frameRate: 8,
    repeat: -1
  });
  this.anims.create({
    key: 'boss1fireball',
    frames: this.anims.generateFrameNumbers('boss1fireball', { frames: [0, 1, 2, 3] }),
    frameRate: 8,
    repeat: -1
  });
  this.anims.create({
    key: 'fx-boss1explosion',
    frames: this.anims.generateFrameNumbers('fx-boss1explosion', { start: 0, end: 7 }),
    frameRate: 8
  });
  this.anims.create({
    key: 'bomb-walk',
    frames: this.anims.generateFrameNumbers('walk-bomb', { frames: [0, 1, 2] }),
    frameRate: 8,
    repeat: -1
  });
  this.anims.create({
    key: 'fx-sword',
    frames: this.anims.generateFrameNumbers('fx-sword', { start: 0, end: 5 }),
    frameRate: 18,
    repeat: 1
  });
  this.anims.create({
    key: 'fx-arx-force',
    frames: this.anims.generateFrameNumbers('fx-arx-force', { start: 0, end: 7 }),
    frameRate: 22,
    repeat: 0
  });
  this.anims.create({
    key: 'fx-bomb-explode',
    frames: this.anims.generateFrameNumbers('fx-bomb-explode', { start: 0, end: 9 }),
    frameRate: 22,
    repeat: 0
  });
  this.anims.create({
    key: 'fx-coin',
    frames: this.anims.generateFrameNumbers('fx-coin', { start: 0, end: 7 }),
    frameRate: 22,
    repeat: -1
  });
  this.anims.create({
    key: 'special-flash',
    frames: this.anims.generateFrameNumbers('special-fill', { frames: [0, 1, 1, 1] }),
    frameRate: 4,
    repeat: -1
  });

  if (!isGM) {
    this.add.sprite(8, 292, 'health-bar').setOrigin(0, 1).setScrollFactor(0).setDepth(30000);
    this.add.sprite(380, 280, 'mute').setScrollFactor(0).setDepth(30000).setInteractive()
      .addListener('pointerup', () => { this.sound.volume = this.sound.volume ? 0 : 1; console.log(`volume: ${this.sound.volume}`) });
  }

  this.keys = {
    up: { input: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.W), isPressed: false },
    down: { input: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.S), isPressed: false },
    left: { input: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A), isPressed: false },
    right: { input: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D), isPressed: false },
    attack: { input: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.K), isPressed: false },
    special: { input: this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.L), isPressed: false },
  };

  const camera = this.cameras.main;
  camera.setDeadzone(20, 20);

  this.socket = io();
  this.socket.on('connected', update => game.playerId = update.id);
  this.socket.on('update', update => game.update(update));
  this.socket.on('updateStatic', update => game.updateStatic(update));
  if (isGM) {
    window.gmfunc = msg => this.socket.emit('gmfunc', msg);
  }
}
 
function update() {
  let change = false;
  game.localUpdate();

  switch (game.mode) {
    case 'menu':
      if (game.selectedCharacter) {
        this.socket.emit('playerSelect', {character: game.selectedCharacter});
        game.setMode('game');
      }
      break;
    case 'game':
      Object.values(this.keys).forEach(key => {
          const isPressed = key.input.isDown;
          change = change || isPressed != key.isPressed;
          key.isPressed = isPressed;
        }
      );
      if (change) {
        const event = {};
        Object.keys(this.keys).forEach(key => event[key] = this.keys[key].isPressed);
        this.socket.emit('playerInput', event);
      }
      break;
    case 'gm':
      game.gmView = {
        x: game.gmView.x + (this.keys.right.input.isDown ? 10 : 0) - (this.keys.left.input.isDown ? 10 : 0),
        y: game.gmView.y + (this.keys.down.input.isDown ? 10 : 0) - (this.keys.up.input.isDown ? 10 : 0),
      };
  }
}

function inverseLinearTween(duration) {
  return v => 1 - Math.min(duration, v) / duration;
}

class Tweener {
  constructor (tweenFn, callbackFn) {
    const startTime = Date.now();
    this.interval = setInterval(() => callbackFn(tweenFn((Date.now() - startTime) / 1000)), 100);
  }

  dispose() {
    clearInterval(this.interval);
  }
}