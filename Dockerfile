# syntax=docker/dockerfile:1.0

FROM node:12-alpine

COPY --chown=node:node public /home/node/app/public
COPY --chown=node:node server /home/node/app/server
COPY --chown=node:node package.json /home/node/app
WORKDIR /home/node/app
RUN npm i
USER node
EXPOSE 8081
CMD npm start